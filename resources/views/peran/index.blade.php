@extends('layout.master')

@section('judul')
    Halaman Peran
@endsection

@section('content')
    <a href="{{ route('admin.peran.create') }}" class="btn-warning btn-sm mb-3">Tambah Data</a>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($peran as $key => $item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $item->peran }}</td>
                    <td>
                        <form action="{{ route('admin.genre.delete', ['id' => $item->id]) }}" method="POST">
                            <a href="{{ route('admin.genre.show', ['id' => $item->id]) }}"
                                class="btn btn-primary btn-sm">Detail</a>
                            @method('delete')
                            <a href="{{ route('admin.genre.edit', ['id' => $item->id]) }}"
                                class="btn btn-info btn-sm">Edit</a>
                            @csrf
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">Data Masih Kosong</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
