@extends('layout.master')

@section('judul')
    Halaman Edit Peran
@endsection

@section('content')
    <form action="{{ route('admin.peran.store') }}" method="POST">
        @csrf
        <div class="mb-3">
            <label>Nama Peran</label>
            <input type="text" value="{{$peran->nama}}" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="mb-3">
            <label>Cast</label>
            <select name="cast_id" class="form-control" id="">
                <option value="">---Pilih Cast---</option>
                @foreach ($cast as $item)
                    @if ($item->id===$peran->cast_id)
                        <option value="{{ $item->id }}"selected>{{ $item->nama }}</option>
                    @else
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        @error('cast_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="mb-3">
            <label>Film</label>
            <select name="film_id" class="form-control" id="">
                <option value="">---Pilih Film---</option>
                @foreach ($film as $item)
                    <option value="{{ $item->id }}">{{ $item->judul }}</option>
                @endforeach
            </select>
        </div>
        @error('film_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection