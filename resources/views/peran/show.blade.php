@extends('layout.master')

@section('judul')
    Halaman Detail Peran {{$peran->nama}}
@endsection

@section('content')

<h1>{{$peran->nama}}</h1>
<p>{{$peran->peran}}</p>

<a href="/peran" class="btn btn-secondary">kembali</a>

@endsection