@extends('layout.master')

@section('judul')
    Halaman Edit Cast {{ $cast->nama }}
@endsection

@section('content')
    <form action="{{ route('admin.cast.update', ['id' => $cast->id]) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label>Nama Cast</label>
            <input type="text" name="nama" value="{{ $cast->nama }}" class="form-control" id="exampleInputEmail1"
                aria-describedby="emailHelp">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="mb-3">
            <label>Umur Cast</label>
            <input type="text" name="umur" value="{{ $cast->umur }}" class="form-control" id="exampleInputEmail1"
                aria-describedby="emailHelp">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="mb-3">
            <label>Bio</label>
            <textarea name="bio" class="form-control" cols="30" rows="10">{{ $cast->bio }}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection
