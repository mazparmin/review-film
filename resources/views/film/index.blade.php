@extends('layout.master')
@section('judul')
    List Film
@endsection

@section('content')
    <a href="{{ route('admin.film.create') }}" class="btn-warning btn-sm mb-3">Tambah Data</a>
    <div class="row">
        @forelse ($film as $item)
        <div class="col=4">
        <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="..." alt="Card image cap">
        <div class="card-body">
            <h3>{{$item->judul}}</h3>
            <p class="card-text">{{$item->ringkasan}}</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
        </div>
        </div>
    </div>
        @empty

        @endforelse
        
    </div>
@endsection
