@extends('layout.master')
@section('judul')
Form Pemain Film Baru
@endsection


@section('content')
<form action="{{ route('admin.film.store')}}" method="POST" enctype="multipart/form-data">
@csrf
<div class="form-group">
  <label >Judul Film</label>
  <input type="text" name="judul" class="form-control">
</div>
@error('judul')
<div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
        <label>Ringkasan</label>
        <textarea name="ringkasan" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('ringkasan')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Tahun</label>
      <input type="text" name="tahun" class="form-control">
    </div>
    @error('tahun')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror 
    <div class="form-group">
      <label>Poster</label>
      <input type="file" name="poster" class="form-control">
    </div>
    @error('poster')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-grup">
      <label>Genre</label>
      <select name="genre_id" id="" class="form-control">
        <option value="">====Pilih Genre====</option>
        @foreach ($genre as $item)
          <option value="{{$item->id}}">{{$item->nama}}</option>
        @endforeach
      </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection