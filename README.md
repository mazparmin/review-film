
## Kelompok 7
# REVIEW FILM
### Nama Anggota:
1. Ahmad Miftahul Huda
2. Nibras Alfaruqiyah
3. Suparmin
Tema: Review Film

### Diagram ER :
![DIagram ER](https://gitlab.com/mazparmin/review-film/-/blob/master/ERD.png)
  
### Link Video:
<iframe width="560" height="315" src="https://www.youtube.com/embed/JsiFMaiqVs8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
### Link Deploy: http://trisasoft.com/

[Link Deploy](https://trisasoft.com).

